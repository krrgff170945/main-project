package lab_1_9;

public class prog1_9
{

	public static void text(String str1)//print in console
	{
		System.out.print(str1);
	}

	public static void textln()
	{
		System.out.println();
	}

	public static void replace(int[] array1, int number1, int[] array2, int number2)//replaceelements of arrays
	{
		array1[number1] += array2[number2];
		array2[number2] = array1[number1] - array2[number2];
		array1[number1] -= array2[number2];
	}

	public static void arrayOut(int[] array1)//print array
	{
		for(int i = 0; i < array1.length; i++)
		{
			System.out.print(array1[i]);
			System.out.print("  ");
		}
	}

	public static void sort(int[] array1)
	{
		int length = array1.length;

		for(int i = 0; i < length / 2; i++)
			replace(array1, i, array1, length - 1 - i);
	}

	public static void main(String args[])
	{
		int[] userArray = {15, 10, 51, -6, -5, 3, -10, -34, 0, 32, 56, -12, 24, -52};
		arrayOut(userArray);
		sort(userArray);
		textln();
		arrayOut(userArray);

	}
}
