package lab_1_2;

import java.util.Scanner;

public class prog1_2
{

	public static int read()
	{
		int variable = 0;

		Scanner in = new Scanner(System.in);
		variable = in.nextInt();
		in.close();

		return variable;
	}

	public static void text(String str1)//print in console
	{
		System.out.print(str1);
	}

        public static void textln()//print in console
        {
                System.out.print("");
        }

	public static int sumOfNumbers(int number)
	{

		int sum = 0;
		int hung = number / 100;
		int dec = (number - hung*100) / 10;
		int one = number - dec*10 - hung*100;

		sum = dec + hung + one;

		return sum;
	}

        public static void main(String args[])
	{
		int number = 0;

		text("enter number: ");
		number = read();
		number = sumOfNumbers(number);
		text("Your number is:" + number);

	}
}
