package lab_1_8;

public class prog1_8
{

	public static int sumOfPositive(int[] array1)
	{
		int sum = 0;

		for(int i = 0; i < array1.length; i++)
			if(array1[i] > 0)
				sum += array1[i];

		return sum;
	}

        public static int sumOfNegative(int[] array1)
        {
                int sum = 0;

                for(int i = 0; i < array1.length; i++)
                        if(array1[i] < 0)
                                sum += array1[i];

                return sum;
        }


	public static int sumOfEvenNegative(int[] array1)
        {
                int sum = 0;

                for(int i = 0; i < array1.length; i++)
                        if((array1[i] < 0) && (array1[i] %2 == 0))
                                sum += array1[i];

                return sum;
        }

        public static int averageOfNegative(int[] array1)
        {
                int average = 0;

                average = sumOfNegative(array1) / numberOfNegative(array1);

                return average;
        }


        public static int numberOfPositive(int[] array1)
        {
                int num = 0;

                for(int i = 0; i < array1.length; i++)
                        if(array1[i] > 0)
                                num ++;

                return num;
        }

        public static int numberOfNegative(int[] array1)
        {
                int num = 0;

                for(int i = 0; i < array1.length; i++)
                        if(array1[i] < 0)
                                num ++;

                return num;
        }


	public static int max(int[] array1)//maximum element of array
	{
		int max = array1[0];

		for(int i = 0; i < array1.length; i++)
			if(array1[i] > max)
				max = array1[i];
		
		return max;
	}

	public static void text(String str1)//print in console
	{
		System.out.print(str1);
	}

	public static void textln()
	{
		System.out.println();
	}

	public static void arrayOut(int[] array1)//print array
	{
		for(int i = 0; i < array1.length; i++)
		{
			System.out.print(array1[i]);
			System.out.print("  ");
		}
	}

	public static void main(String args[])
	{
		int[] userArray = {1, -10, 5, 6, 45, 23, -45, -34, 0, 32, 56, -1, 2, -2};
		arrayOut(userArray);
		textln();
		text( "max is: " + max(userArray) );
		textln();
		text( "sum of positive is: " + sumOfPositive(userArray) );
		textln();
		text( "sum of even negative is: " + sumOfEvenNegative(userArray) );
		textln();
		text( "number of positive is: " + numberOfPositive(userArray) );
		textln();
		text( "average of negative is: " + averageOfNegative(userArray) );

	}
}
