package lab_1_1;

import java.util.Scanner;

public class prog1_1
{

	public static int read()
	{
		int variable = 0;

		Scanner in = new Scanner(System.in);
		variable = in.nextInt();
		in.close();

		return variable;
	}

	public static void text(String str1)//print in console
	{
		System.out.print(str1);
	}

        public static void textln()//print in console
        {
                System.out.print("");
        }

	public static int lastChar(int number)
	{
		int num1 = 0;
		int len = 0;
		String str1 = "";
		str1 = Integer.toString(number);
		len = str1.length();
		str1 = str1.substring(len - 1);
		num1 = Integer.parseInt(str1);

		return num1;
	}

        public static void main(String args[])
	{
		int number = 0;

		text("enter number: ");
		number = read();
		number = lastChar(number);
		text("Your number is:" + number);

	}
}
