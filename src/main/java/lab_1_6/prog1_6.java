package lab_1_6;

import java.util.Scanner;

public class prog1_6
{

	public static int read()
	{
		int variable = 0;

		Scanner in = new Scanner(System.in);
		variable = in.nextInt();
		in.close();

		return variable;
	}

	public static void text(String str1)//print in console
	{
		System.out.print(str1);
	}

        public static void textln()//print in console
        {
                System.out.print("");
        }

	public static String isPositiveOrNegative(int number)
	{

		String string1 = "";

		if(number < 0)
			string1 = " Negative ";

		if(number == 0)
			string1 = " Null ";

		if(number > 0)
			string1 = " Positive ";

		return string1;
	}

        public static String isEven(int number)
        {

		String string1 = "";

                if((number %2 == 0) && (number != 0))
                        string1 = " Even ";

                if(number %2 != 0)
                        string1 = " Not Even ";

		return string1;

        }


	public static void main(String args[])
	{
		int number = 0;
		String description = "";

		text("enter number: ");
		number = read();
		description += isEven(number);
		description += isPositiveOrNegative(number);
		text("Your number is:" + description);

	}
}
